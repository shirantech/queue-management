<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom User Defined Constants
    |--------------------------------------------------------------------------
    |
    | Usage : config('filename.key')
    |
    |
    */
    'default_time_zone' => 'Asia/Kathmandu',
    'current_date' => \Carbon\Carbon::parse()->timezone('Asia/Kathmandu')->format('Y-m-d'),
    'current_date_time' => \Carbon\Carbon::parse()->timezone('Asia/Kathmandu')->format('Y-m-d H:i:s'),
    'max_total_records' => 20,
    'max_active_records' => 4,
    'hospital_name' => 'Cogent Queue Management',
    'first_menu' => 'Consultants',
    'second_menu' => 'Pharmacy',
    'third_menu' => 'Laboratory',
    'forth_menu' => 'Radiology'

];