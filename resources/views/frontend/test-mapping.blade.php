@extends('frontend.layouts.master')

@section('content')
    <div id="wrapper">


        <div class="content">


            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-body">
                            <a href="{{route('add.test.mapping')}}" class="btn btn-primary btn-sm">Add</a>
                            @if(Session::get('success_message'))
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span
                                            class="sr-only">Close</span></button>
                                    {!! Session::get('success_message') !!}
                                </div>
                            @endif

                            @if(Session::get('error_message'))
                                <div class="alert alert-danger containerAlert">
                                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span
                                            class="sr-only">Close</span></button>
                                    {{ Session::get('error_message') }}
                                </div>
                            @endif
                            <br>
                            <div class="table-responsive">
                                <table cellpadding="1" cellspacing="1" class="table table-bordered">
                                    <thead>
                                    <tr style="background: #dedede;color: #060606;">
                                        <th>SNo</th>
                                        <th>Code</th>
                                        <th>Test</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($tests))
                                        @foreach($tests as $test)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $test->code }}</td>
                                                <td>{{ $test->test_name }}</td>
                                                <td>
                                                    <a href="{{ route('edit.test.mapping', $test->id) }}"><i class="fa fa-pencil text-info"></i></a>
                                                    <a href="{{ route('delete.test.mapping', $test->id) }}" onclick="return confirm('Delete?')"><i class="fa fa-trash text-danger"></i></a>

                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
@stop
