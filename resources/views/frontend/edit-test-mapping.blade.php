@extends('frontend.layouts.master')

@section('content')
    <div id="wrapper">

        <div class="content">

            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-body">
                            <form action="{{ route('update.test.mapping') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <input type="hidden" name="_id" value="{{ $test->id }}">
                                    <label for="test_code">Test Code</label>
                                    <input type="text" class="form-control" name="test_code" id="test_code" aria-describedby="emailHelp" placeholder="Code" value="{{ $test->code }}">
                                    <small class="help-block text-danger">{{$errors->first('test_code')}}</small>
                                </div>
                                <div class="form-group">
                                    <label for="test_name">Test Name</label>
                                    <input type="text" name="test_name" class="form-control" id="test_name" placeholder="Name" value="{{ $test->test_name }}">
                                    <small class="help-block text-danger">{{$errors->first('test_name')}}</small>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-1 align-self-center mb-0" for="">Active:</label>
                                    <div class="col-sm-3">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" name="status" class="custom-control-input" {{ $test->status == 1 ?'checked=""':'' }} value="1">
                                            <label class="custom-control-label" for="">True</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" name="status" class="custom-control-input" {{ $test->status == 0 ?'checked=""':'' }} value="0">
                                            <label class="custom-control-label" for="">False</label>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
@stop
