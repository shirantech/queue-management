@extends('frontend.layouts.master')

@section('content')

<div id="wrapper">

    <style>
        .btn_right {
            text-align: right;
        }
    </style>
    <style>
        .panel-body tr td:nth-child(1) {
            min-width: 230px;
        }

        .panel-body tr td:nth-child(2) {
            min-width: 450px;
        }

        .panel-body tr td:nth-child(3) {
            min-width: 100px;
        }

        .panel-body tr td:nth-child(4) {
            min-width: 120px;
        }



        .table tr th {
            text-transform: uppercase;
        }

        .table tr td {
            font-weight: 600;
            line-height: 25px;
            text-transform: uppercase;
        }
    </style>
    <div class="content">


        <div class="row">
            @if($sound == 'on')
            <audio src="{{ asset('announcement.mp3')}}" id="my_audio" autoplay="autoplay"></audio>
            @endif
            <div class="col-lg-12">
                <div class="hpanel">
                    {{-- <div class="panel-heading">
                            Consultant
                        </div>--}}
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table cellpadding="1" cellspacing="1" class="table table-bordered">
                                <thead>
                                    <tr style="background: #dedede;color: #060606;">

                                        <th>Enc Id</th>
                                        <th>Name</th>
                                        <th>Department</th>
                                        <th>Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($lists->count() > 0)
                                    @foreach($lists as $k => $l)
                                    <tr class="{{ $l->fldinside == 1 ? 'active_tr' : '' }}">

                                        <td>{{ $l->fldencounterval }}</td>
                                        <td>{{ $l->fldptnamefir }} {{ $l->fldptnamelast }}</td>
                                        <td>{{ $l->department }}</td>
                                        <?php $date = date('Y-m-d', strtotime($l->fldordtime)); ?>
                                        <td><?php $dateNep =  Helpers::dateEngToNep($date);
                                            echo $dateNep->year . '-' . $dateNep->month . '-' . $dateNep->date . ' ' . date('H:i:s', strtotime($l->fldordtime))  ?></td>


                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

    <!-- Right sidebar -->
    <div id="right-sidebar" class="animated fadeInRight">
        <div class="p-m">
            <div class="btn_right">
                <button id="sidebar-close" class="right-sidebar-toggle sidebar-button btn btn-default m-b-md">
                    <i class="fa fa-times"></i>
                </button>
            </div>

            <div class="row">
                <form method="get">


                    <div class="form-group">
                        <div class="row">
                        <div class="col-md-12">
                                <h5 class="font-bold text-info"> Billing Mode </h5>
                            </div>
                            @if($billing_mode)
                            @foreach($billing_mode as $billing)
                            <div class="col-md-3"><label> <input type="checkbox" name="all_billing[]"  @if(in_array($billing->fldsetname,$fldsetname)) checked @endif value="{{ $billing->fldsetname }}"> {{ $billing->fldsetname }} </label></div>
                            @endforeach
                            @endif

                            <div class="col-md-12">
                                <h5 class="font-bold text-info"> All Departments </h5>
                            </div>
                            @if($departments)
                            @foreach($departments as $depart)
                            <div class="col-md-3"><label> <input type="checkbox" name="all_departments[]" @if(in_array($depart->fldadmitlocat,$fldadmitlocat)) checked @endif value="{{ $depart->fldadmitlocat }}"> {{ $depart->fldadmitlocat }} </label></div>
                            @endforeach
                            @endif


                            <div class="col-md-12">
                                <h5 class="font-bold text-info">All Targets </h5>
                            </div>
                            @if($targets)
                            @foreach($targets as $target)
                            <div class="col-md-3"><label> <input type="checkbox" name="all_targets[]"  @if(in_array($target->fldtarget,$fldtarget)) checked @endif value="{{ $target->fldtarget }}"> {{ $target->fldtarget }} </label></div>
                            @endforeach
                            @endif


                            

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10">
                            <button class="btn btn-primary" type="submit">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
            <input type='hidden' id="sound" value="{{ $sound }}" />
            <input type='hidden' id="url" value="{{ route('laboratory') }}" />
        </div>
    </div>

</div>
<script type="text/javascript">
    $(document).ready(function() {
        if ($('#sound').val() === 'on') {
            document.getElementById("my_audio").play();
        }

        var url = $('#url').val();


         setInterval(function() {

             location.reload(true);

         }, 5000);


    });
</script>
@stop