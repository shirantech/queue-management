<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/', 'PatientController@consults')->name('consultants');

Route::get('/pharmacy', 'PatientController@pharmacy')->name('pharmacy');

Route::get('/laboratory', 'PatientController@laboratory')->name('laboratory');

Route::get('/radiology', 'PatientController@radiology')->name('radiology');

Route::get('/machine-interfacing', 'MachineInterfacingController@machineInterfacing')->name('machine.interfacing.get');
Route::get('/cron/{machine_name}', 'MachineInterfacingController@machineInterfacingCron')->name('machine.interfacing.auto');
Route::get('/crondepartment', 'MachineInterfacingController@machineInterfacingDepartment')->name('machine.interfacing.department');
Route::post('/machine-interfacing/submit', 'MachineInterfacingController@machineInterfacingSave')->name('machine.interfacing.post');

Route::get('/test-mapping', 'TestMappingController@listTests')->name('test.mapping');
Route::get('/add-test-mapping', 'TestMappingController@addTestMapping')->name('add.test.mapping');
Route::get('/edit-test-mapping/{id}', 'TestMappingController@editTestMapping')->name('edit.test.mapping');
Route::post('/create-test-mapping', 'TestMappingController@createTestMapping')->name('create.test.mapping');
Route::post('/update-test-mapping', 'TestMappingController@updateTestMapping')->name('update.test.mapping');
Route::get('/delete-test-mapping/{id}', 'TestMappingController@deleteTestMapping')->name('delete.test.mapping');
