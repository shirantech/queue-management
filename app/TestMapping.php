<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestMapping extends Model
{
    protected $table = 'test_mapping';
    protected $guarded = [];
}
