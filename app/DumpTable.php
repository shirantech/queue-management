<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DumpTable extends Model
{
    protected $table = "tbl_dump";
    public $timestamps = false;
    protected $guarded = [];
}
