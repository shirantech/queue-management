<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use Session;
use Illuminate\Support\Facades\DB;

class PatientController extends Controller
{
    public function index()
    {
        $data = [];
        return view('frontend/index', $data);
    }

    public function consults(Request $request)
    {


        $data = array();
        $v = '';
        $data['sound'] = 'off';
        $status = 'Planned';
        $limit = config('constants.max_total_records');
        $from_date = Carbon::parse(config('constants.current_date'))->setTime(0, 0, 0);
        $to_date = Carbon::parse(config('constants.current_date'))->setTime(23, 59, 59);

        $all_consultants = DB::table('tblconsult')
            ->join('tblencounter', 'tblencounter.fldencounterval', '=', 'tblconsult.fldencounterval')
            ->join('tblpatientinfo', 'tblpatientinfo.fldpatientval', '=', 'tblencounter.fldpatientval')
            ->select('tblconsult.*', 'tblconsult.fldid as confldid', 'tblpatientinfo.*', 'tblencounter.*', 'tblconsult.flduserid as consultant')
            ->where('tblconsult.fldstatus', $status)
            ->where('tblconsult.fldconsulttime', '>=', $from_date)
            ->where('tblconsult.fldconsulttime', '<=', $to_date)
            ->orderBy('tblencounter.fldinside', 'desc')
            ->orderBy('tblconsult.fldconsulttime', 'asc')
            ->orderBy('tblconsult.fldconsulttime', 'asc')
            ->limit($limit);

        $inside_all_consultants = DB::table('tblconsult')
            ->join('tblencounter', 'tblencounter.fldencounterval', '=', 'tblconsult.fldencounterval')
            ->join('tblpatientinfo', 'tblpatientinfo.fldpatientval', '=', 'tblencounter.fldpatientval')
            ->select('tblconsult.*', 'tblconsult.fldid as confldid', 'tblpatientinfo.*', 'tblencounter.*', 'tblconsult.flduserid as consultant')
            ->where('tblconsult.fldstatus', $status)
            ->where('tblencounter.fldinside', 1)
            ->where('tblconsult.fldconsulttime', '>=', $from_date)
            ->where('tblconsult.fldconsulttime', '<=', $to_date)
            ->orderBy('tblencounter.fldinside', 'desc')
            ->orderBy('tblconsult.fldconsulttime', 'asc')
            ->orderBy('tblconsult.fldconsulttime', 'asc')
            ->limit($limit);





        $consult_name = [];
        if ($request->has('all_departments')) {
            $consult_name = $request->get('all_departments');

            $all_consultants->whereIn('tblconsult.fldconsultname', $consult_name);
            $inside_all_consultants->whereIn('tblconsult.fldconsultname', $consult_name);
        }

        $fldsetname = [];
        if ($request->has('all_billing')) {
            $fldsetname = $request->get('all_billing');

            $all_consultants->whereIn('tblencounter.fldbillingmode', $fldsetname);
            $inside_all_consultants->whereIn('tblencounter.fldbillingmode', $fldsetname);
        }


        $data['lists'] = $all_consultants->get();
        $data['inside_lists'] = $inside_all_consultants->get();


        if ($data['lists']) {
            $set_limit = array();
            foreach ($data['lists'] as $k => $consult) {

                $set_limit[$k] = $consult->confldid;
            }

            $set_limit_inside = array();
            foreach ($data['inside_lists'] as $k => $consults) {

                $set_limit_inside[$k] = $consults->confldid;
            }

            $v =  session('lastest_fldencounterval');
            $in =  session('lastest_fldencounterval_inside');



            if ((!empty($set_limit) && !empty($v) && $set_limit != $v) || (!empty($set_limit_inside) && !empty($in) && $set_limit_inside != $in)) {

                $data['sound'] = 'on';
            } else {
                $data['sound'] = 'off';
            }

            Session::put('lastest_fldencounterval', $set_limit);
            Session::put('lastest_fldencounterval_inside', $set_limit_inside);
        }

        $data['consult_name'] = $consult_name;
        $data['fldsetname'] = $fldsetname;
        $data['departments'] = DB::table('tbldepartment')->get();
        $data['billing_mode'] = DB::table('tblbillingset')->get();
//         $lang = "ne-IN";
//         $text = 'Encounter no SBHF1234+1 kripaya prastan garnus hos';
// $mp3 = file_get_contents(
// 'https://translate.google.com/translate_tts?ie=UTF-8&q='. urlencode($text) .'&tl='. $lang .'&client=gtx');

        return view('frontend/index', $data);
    }

    public function pharmacy(Request $request)
    {

        $data = array();
        $v = '';
        $data['sound'] = 'off';
        $status = 'Continue';
        $limit = config('constants.max_total_records');
        $from_date = Carbon::parse(config('constants.current_date'))->setTime(0, 0, 0);
        $to_date = Carbon::parse(config('constants.current_date'))->setTime(23, 59, 59);

        $all_consultants = DB::table('tblpatdosing')
            ->join('tblencounter', 'tblencounter.fldencounterval', '=', 'tblpatdosing.fldencounterval')
            ->join('tblpatientinfo', 'tblpatientinfo.fldpatientval', '=', 'tblencounter.fldpatientval')
            ->join('tblmacaccess', 'tblmacaccess.fldcomp', '=', 'tblpatdosing.fldcomp_order')
            ->select('tblpatdosing.*', 'tblpatdosing.fldid as confldid', 'tblpatientinfo.*', 'tblencounter.*', 'tblpatdosing.flduserid as consultant', 'tblmacaccess.fldcompname as maccompname')
            ->where('tblpatdosing.fldsave_order', False)
            ->where('tblpatdosing.fldsave', True)
            ->where('tblpatdosing.fldorder', 'Request')
            ->where('tblpatdosing.fldcurval', $status)
            ->where('fldtime_order', '>=', $from_date)
            ->where('fldtime_order', '<=', $to_date)
            ->groupBy('tblpatdosing.fldencounterval')
            ->orderBy('tblencounter.fldphminside', 'desc')
            ->orderBy('fldtime_order', 'asc')
            
            ->limit($limit);

        $inside_all_consultants = DB::table('tblpatdosing')
            ->join('tblencounter', 'tblencounter.fldencounterval', '=', 'tblpatdosing.fldencounterval')
            ->join('tblpatientinfo', 'tblpatientinfo.fldpatientval', '=', 'tblencounter.fldpatientval')
            ->join('tblmacaccess', 'tblmacaccess.fldcomp', '=', 'tblpatdosing.fldcomp_order')
            ->select('tblpatdosing.*', 'tblpatdosing.fldid as confldid', 'tblpatientinfo.*', 'tblencounter.*', 'tblpatdosing.flduserid as consultant', 'tblmacaccess.fldcompname as maccompname')
            ->where('tblpatdosing.fldsave_order', False)
            ->where('tblpatdosing.fldsave', True)
            ->where('tblpatdosing.fldorder', 'Request')
            ->where('tblpatdosing.fldcurval', $status)
            ->where('tblencounter.fldinside', 1)
            ->where('fldtime_order', '>=', $from_date)
            ->where('fldtime_order', '<=', $to_date)
            ->groupBy('tblpatdosing.fldencounterval')
            ->orderBy('tblencounter.fldphminside', 'desc')
            ->orderBy('fldtime_order', 'asc')
            
            ->limit($limit);


        $fldcomp_order = [];
        if ($request->has('all_departments')) {
            $fldcomp_order = $request->get('all_departments');

            $all_consultants->whereIn('tblpatdosing.fldcomp_order', $fldcomp_order);
            $inside_all_consultants->whereIn('tblpatdosing.fldcomp_order', $fldcomp_order);
        }

        $fldsetname = [];
        if ($request->has('all_billing')) {
            $fldsetname = $request->get('all_billing');

            $all_consultants->whereIn('tblencounter.fldbillingmode', $fldsetname);
            $inside_all_consultants->whereIn('tblencounter.fldbillingmode', $fldsetname);
        }




        $data['lists'] = $all_consultants->get();
        $data['inside_lists'] = $inside_all_consultants->get();

        if ($data['lists']) {
            $set_limit = array();
            foreach ($data['lists'] as $k => $consult) {

                $set_limit[$k] = $consult->confldid;
            }

            $set_limit_inside = array();
            foreach ($data['inside_lists'] as $k => $consults) {

                $set_limit_inside[$k] = $consults->confldid;
            }

            $v =  session('lastest_fldencounterval');
            $in =  session('lastest_fldencounterval_inside');



            if ((!empty($set_limit) && !empty($v) && $set_limit != $v) || (!empty($set_limit_inside) && !empty($in) && $set_limit_inside != $in)) {

                $data['sound'] = 'on';
            } else {
                $data['sound'] = 'off';
            }

            Session::put('lastest_fldencounterval', $set_limit);
            Session::put('lastest_fldencounterval_inside', $set_limit_inside);
        }

        $data['fldcomp_order'] = $fldcomp_order;
        $data['departments'] = DB::table('tblmacaccess')->get();
        $data['billing_mode'] = DB::table('tblbillingset')->get();
        $data['fldsetname'] = $fldsetname;

        return view('frontend/pathology', $data);
    }


    public function laboratory(Request $request)
    {

        $data = array();
        $v = '';
        $data['sound'] = 'off';
        $status = 'Waiting';
        $limit = config('constants.max_total_records');
        $from_date = Carbon::parse(config('constants.current_date'))->setTime(0, 0, 0);
        $to_date = Carbon::parse(config('constants.current_date'))->setTime(23, 59, 59);

        $all_consultants = DB::table('tblpatbilling')
            ->join('tblencounter', 'tblencounter.fldencounterval', '=', 'tblpatbilling.fldencounterval')
            ->join('tblpatientinfo', 'tblpatientinfo.fldpatientval', '=', 'tblencounter.fldpatientval')
            ->select('tblpatbilling.*', 'tblpatbilling.fldid as confldid', 'tblpatientinfo.*', 'tblencounter.*', 'tblencounter.fldadmitlocat as department')
            ->where('tblpatbilling.flditemtype', 'Diagnostic Tests')
            ->where('tblpatbilling.fldsave', True)
            ->where('tblpatbilling.fldsample', $status)
            ->where('tblpatbilling.fldordtime', '>=', $from_date)
            ->where('tblpatbilling.fldordtime', '<=', $to_date)
            ->groupBy('tblpatbilling.fldencounterval')
            ->orderBy('tblencounter.fldinside', 'desc')
            ->orderBy('tblpatbilling.fldordtime', 'asc')
            
            ->limit($limit);

        $inside_all_consultants = DB::table('tblpatbilling')
            ->join('tblencounter', 'tblencounter.fldencounterval', '=', 'tblpatbilling.fldencounterval')
            ->join('tblpatientinfo', 'tblpatientinfo.fldpatientval', '=', 'tblencounter.fldpatientval')
            ->select('tblpatbilling.*', 'tblpatbilling.fldid as confldid', 'tblpatientinfo.*', 'tblencounter.*', 'tblencounter.fldadmitlocat as department')
            ->where('tblpatbilling.flditemtype', 'Diagnostic Tests')
            ->where('tblpatbilling.fldsave', True)
            ->where('tblpatbilling.fldsample', $status)
            ->where('tblencounter.fldinside', 1)
            ->where('tblpatbilling.fldordtime', '>=', $from_date)
            ->where('tblpatbilling.fldordtime', '<=', $to_date)
            ->groupBy('tblpatbilling.fldencounterval')
            ->orderBy('tblencounter.fldinside', 'desc')
            ->orderBy('tblpatbilling.fldordtime', 'asc')
            
            ->limit($limit);


        $fldadmitlocat = [];
        if ($request->has('all_departments')) {
            $fldadmitlocat = $request->get('all_departments');
            if (in_array('Indoor', $fldadmitlocat)) {
                $indorlist = DB::table('tbldepartmentbed')->select('fldbed')
                    ->where('tbldepartmentbed.flddept', 'Indoor')->get()->toArray();
                $indorlistarr = array();
                if ($indorlist) {

                    foreach ($indorlist as $k => $indor) {
                        $indorlistarr[$k] = $indor->fldbed;
                    }
                }

                $all_dept = array_merge($indorlistarr, $fldadmitlocat);


                

                $all_consultants->whereIn('tblencounter.fldadmitlocat', $all_dept);
                $inside_all_consultants->whereIn('tblencounter.fldadmitlocat', $all_dept);
            } else {
                $all_consultants->whereIn('tblencounter.fldadmitlocat', $fldadmitlocat);
                $inside_all_consultants->whereIn('tblencounter.fldadmitlocat', $fldadmitlocat);
            }
        }

        $fldtarget = [];
        if ($request->has('all_targets')) {
            $fldtarget = $request->get('all_targets');

            $all_consultants->whereIn('tblpatbilling.fldtarget', $fldtarget);
            $inside_all_consultants->whereIn('tblpatbilling.fldtarget', $fldtarget);
        }

        $fldsetname = [];
        if ($request->has('all_billing')) {
            $fldsetname = $request->get('all_billing');

            $all_consultants->whereIn('tblencounter.fldbillingmode', $fldsetname);
            $inside_all_consultants->whereIn('tblencounter.fldbillingmode', $fldsetname);
        }






        $data['lists'] = $all_consultants->get();
        $data['inside_lists'] = $inside_all_consultants->get();


        if ($data['lists']) {
            $set_limit = array();
            foreach ($data['lists'] as $k => $consult) {

                $set_limit[$k] = $consult->confldid;
            }

            $set_limit_inside = array();
            foreach ($data['inside_lists'] as $k => $consults) {

                $set_limit_inside[$k] = $consults->confldid;
            }

            $v =  session('lastest_fldencounterval');
            $in =  session('lastest_fldencounterval_inside');



            if ((!empty($set_limit) && !empty($v) && $set_limit != $v) || (!empty($set_limit_inside) && !empty($in) && $set_limit_inside != $in)) {

                $data['sound'] = 'on';
            } else {
                $data['sound'] = 'off';
            }

            Session::put('lastest_fldencounterval', $set_limit);
            Session::put('lastest_fldencounterval_inside', $set_limit_inside);
        }
        //dd($data['lists']);

        $data['fldadmitlocat'] = $fldadmitlocat;
        $data['fldtarget'] = $fldtarget;
        $data['departments'] = DB::table('tblencounter')->select('fldadmitlocat')->groupBy('fldadmitlocat')->get();
        $data['targets'] = DB::table('tblservicecost')->select('fldtarget')->where('fldtarget', 'like', 'comp%')->groupBy('fldtarget')->get();
        $data['billing_mode'] = DB::table('tblbillingset')->get();
        $data['fldsetname'] = $fldsetname;




        return view('frontend/laboratory', $data);
    }


    public function radiology(Request $request)
    {

        $data = array();
        $v = '';
        $data['sound'] = 'off';
        $status = 'Waiting';
        $limit = config('constants.max_total_records');
        $from_date = Carbon::parse(config('constants.current_date'))->setTime(0, 0, 0);
        $to_date = Carbon::parse(config('constants.current_date'))->setTime(23, 59, 59);

        $all_consultants = DB::table('tblpatbilling')
            ->join('tblencounter', 'tblencounter.fldencounterval', '=', 'tblpatbilling.fldencounterval')
            ->join('tblpatientinfo', 'tblpatientinfo.fldpatientval', '=', 'tblencounter.fldpatientval')
            ->select('tblpatbilling.*', 'tblpatbilling.fldid as confldid',  'tblpatientinfo.*', 'tblencounter.*', 'tblencounter.fldadmitlocat as department')
            ->where('tblpatbilling.flditemtype', 'Radio Diagnostics')
            ->where('tblpatbilling.fldsave', True)
            ->where('tblpatbilling.fldsample', $status)
            ->where('tblpatbilling.fldordtime', '>=', $from_date)
            ->where('tblpatbilling.fldordtime', '<=', $to_date)
            ->groupBy('tblpatbilling.fldencounterval')
            ->orderBy('tblencounter.fldinside', 'desc')
            ->orderBy('tblpatbilling.fldordtime', 'asc')
            
            ->limit($limit);

        $inside_all_consultants = DB::table('tblpatbilling')
            ->join('tblencounter', 'tblencounter.fldencounterval', '=', 'tblpatbilling.fldencounterval')
            ->join('tblpatientinfo', 'tblpatientinfo.fldpatientval', '=', 'tblencounter.fldpatientval')
            ->select('tblpatbilling.*', 'tblpatbilling.fldid as confldid',  'tblpatientinfo.*', 'tblencounter.*', 'tblencounter.fldadmitlocat as department')
            ->where('tblpatbilling.flditemtype', 'Radio Diagnostics')
            ->where('tblpatbilling.fldsave', True)
            ->where('tblpatbilling.fldsample', $status)
            ->where('tblencounter.fldinside', 1)
            ->where('tblpatbilling.fldordtime', '>=', $from_date)
            ->where('tblpatbilling.fldordtime', '<=', $to_date)
            ->groupBy('tblpatbilling.fldencounterval')
            ->orderBy('tblencounter.fldinside', 'desc')
            ->orderBy('tblpatbilling.fldordtime', 'asc')
           
            ->limit($limit);


        $fldadmitlocat = [];
        if ($request->has('all_departments')) {
            $fldadmitlocat = $request->get('all_departments');
            if (in_array('Indoor', $fldadmitlocat)) {
                $indorlist = DB::table('tbldepartmentbed')->select('fldbed')
                    ->where('tbldepartmentbed.flddept', 'Indoor')->get()->toArray();
                $indorlistarr = array();
                if ($indorlist) {

                    foreach ($indorlist as $k => $indor) {
                        $indorlistarr[$k] = $indor->fldbed;
                    }
                }

                $all_dept = array_merge($indorlistarr, $fldadmitlocat);




                $all_consultants->whereIn('tblencounter.fldadmitlocat', $all_dept);
                $inside_all_consultants->whereIn('tblencounter.fldadmitlocat', $all_dept);
            } else {
                $all_consultants->whereIn('tblencounter.fldadmitlocat', $fldadmitlocat);
                $inside_all_consultants->whereIn('tblencounter.fldadmitlocat', $fldadmitlocat);
            }
        }

        $fldtarget = [];
        if ($request->has('all_targets')) {
            $fldtarget = $request->get('all_targets');

            $all_consultants->whereIn('tblpatbilling.fldtarget', $fldtarget);
            $inside_all_consultants->whereIn('tblpatbilling.fldtarget', $fldtarget);
        }

        //comp bhako matra target line

        $fldsetname = [];
        if ($request->has('all_billing')) {
            $fldsetname = $request->get('all_billing');

            $all_consultants->whereIn('tblencounter.fldbillingmode', $fldsetname);
            $inside_all_consultants->whereIn('tblencounter.fldbillingmode', $fldsetname);
        }




        $data['lists'] = $all_consultants->get();
        $data['inside_lists'] = $inside_all_consultants->get();
        if ($data['lists']) {
            $set_limit = array();
            foreach ($data['lists'] as $k => $consult) {

                $set_limit[$k] = $consult->confldid;
            }

            $set_limit_inside = array();
            foreach ($data['inside_lists'] as $k => $consults) {

                $set_limit_inside[$k] = $consults->confldid;
            }

            $v =  session('lastest_fldencounterval');
            $in =  session('lastest_fldencounterval_inside');



            if ((!empty($set_limit) && !empty($v) && $set_limit != $v) || (!empty($set_limit_inside) && !empty($in) && $set_limit_inside != $in)) {

                $data['sound'] = 'on';
            } else {
                $data['sound'] = 'off';
            }

            Session::put('lastest_fldencounterval', $set_limit);
            Session::put('lastest_fldencounterval_inside', $set_limit_inside);
        }



        $data['fldadmitlocat'] = $fldadmitlocat;
        $data['fldtarget'] = $fldtarget;
        $data['departments'] = DB::table('tblencounter')->select('fldadmitlocat')->groupBy('fldadmitlocat')->get();
        $data['targets'] = DB::table('tblservicecost')->select('fldtarget')->where('fldtarget', 'like', 'comp%')->groupBy('fldtarget')->get();
        $data['billing_mode'] = DB::table('tblbillingset')->get();
        $data['fldsetname'] = $fldsetname;




        return view('frontend/radiology', $data);
    }
}
