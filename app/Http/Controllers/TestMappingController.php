<?php

namespace App\Http\Controllers;

use App\TestMapping;
use Illuminate\Http\Request;
use Session;

class TestMappingController extends Controller
{
    public function listTests()
    {
        $data['tests'] = TestMapping::where('status', 1)->get();
        return view('frontend/test-mapping', $data);
    }

    public function addTestMapping()
    {
        return view('frontend/add-test-mapping');
    }

    public function createTestMapping(Request $request)
    {
        $request->validate([
            'test_name' => 'required',
            'test_code' => 'required',
        ]);

        try {
            \App\TestMapping::create(['code' => $request->test_code, 'test_name' => $request->test_name, 'status' => $request->status]);
            Session::flash('success_message', 'Test mapping created successfully.');
            return redirect()->route('test.mapping');
        } catch (\Exception $e) {
            Session::flash('error_message', 'Something went wrong.');
            return redirect()->route('test.mapping');
        }

    }

    public function editTestMapping($id)
    {
        $data['test'] = TestMapping::where('id', $id)->first();
        return view('frontend/edit-test-mapping', $data);
    }

    public function updateTestMapping(Request $request)
    {
        $request->validate([
            'test_name' => 'required',
            'test_code' => 'required',
        ]);

        try {
            \App\TestMapping::where('id', $request->_id)->update(['code' => $request->test_code, 'test_name' => $request->test_name, 'status' => $request->status]);
            Session::flash('success_message', 'Test mapping updated successfully.');
            return redirect()->route('test.mapping');
        } catch (\Exception $e) {
            Session::flash('error_message', 'Something went wrong.');
            return redirect()->route('test.mapping');
        }

    }

    public function deleteTestMapping($id)
    {
        try {
            \App\TestMapping::where('id', $id)->delete();
            Session::flash('success_message', 'Test mapping deleted successfully.');
            return redirect()->route('test.mapping');
        } catch (\Exception $e) {
            Session::flash('error_message', 'Something went wrong.');
            return redirect()->route('test.mapping');
        }
    }


}
