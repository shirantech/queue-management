<?php

namespace App\Http\Controllers;

use App\DumpTable;
use Illuminate\Http\Request;
use \DB;

class MachineInterfacingController extends Controller
{
    public function machineInterfacing()
    {
        return view('frontend/machine-interfacing');
    }

    public function machineInterfacingSave(Request $request)
    {
        $validatedData = $request->validate([
            'machine_interfacing' => 'required',
            'machine_interfacing_name' => 'required',
        ]);

        $tbltests = DB::table('tbltest')->pluck('fldtestid')->toArray(); //array('Free thyroxine', 'Free Triiodothyronine', 'Thyyroid Stimulating Hormone','Prostate Specific Antigen');
        $machinename = $request->machine_interfacing_name;

        $header = [];
        $file = fopen($request->machine_interfacing, "r");
        $ara = array();
        $members = array();
        $countDump = 0;
        while (!feof($file)) {
            if ($countDump == 0) {
                $ara[] = preg_split("/[\t]/", fgets($file));
            } else {
                $members[] = fgets($file);
            }
            $countDump++;
        }

        fclose($file);


        $dumpData['machinename'] = $request->machine_interfacing_name;
        $dumpData['header'] = $members[0];
        for ($countData = 0; $countData < count($members); $countData++) {

            try {
                $dumpData['dump'] = $members[$countData];
                if ($dumpData['dump'] != '""	""	""	""	""' || $dumpData['dump'] != 0) {
                    DumpTable::create($dumpData);
                }

                if ($dumpData['machinename'] == "Biochem_sysmex") :
                    $rowdata = preg_split("/[\t]/", $members[$countData]);


                    if ($rowdata) {
                        // print_r($rowdata); echo '<pre>';
                        $testname1 = trim($rowdata[3], '"');
                        $mapping = array(
                            'FT3' => 'Free Triiodothyronine',
                            'FT4' => 'Free thyroxine',
                            'TSH' => 'Thyyroid Stimulating Hormone',
                            'PSA' => 'Prostate Specific Antigen',
                            'Glu' => 'Glucose',
                            'Ure' => 'Urea',
                            'Crea' => 'Creatinine',
                            'ALP' => 'Alkaline Phosphate',
                            'ALT' => 'Alanine Transaminase',
                            'SGPT' => 'Serum Glutamic Pyruvic Transaminase',
                            'AST' => 'Aspartate transaminase',
                            'SGOT' => 'serum glutamic-oxaloacetic transaminase',
                            'TSB' => 'Total serum bilirubin',
                            'DSB' => 'Direct Serum bilirubin',
                            'BBT' => 'Bilirubin Total',
                            'BBD' => 'Bilirubin Direct',
                            'S.ALB' => 'Serum Albumin',
                            'TP' => 'Total Protein',
                            'GGT' => 'Gamma-GT',
                            'CK-MB' => 'Creatine Kinase -MB',
                            'CK-NAC' => 'Creatine Kinase -N',
                            'CALL++' => 'Calcium',
                            'IRON' => 'Iron',
                            'AMY' => 'Amylase',
                            'LIP' => 'Lipase',
                            'TG' => 'Triglyceride',
                            'CHOL' => 'Cholelsterol',
                            'HDL' => 'High Density Lipoprotein',
                            'LDL' => 'low Density Lipoprotein ',
                            'LDH' => 'Lactate Dehydrogenase',
                            'MAG' => 'Magnesium',
                            'U/A' => 'Uric Acid',
                            'FERIT' => 'Ferritin',

                        );

                        if (array_key_exists($testname1, $mapping)) {
                            $testname = $mapping[$testname1];
                        } else {
                            $testname = $testname1;
                        }

                        $search_array = array_map('strtolower', $tbltests);

                        // echo '<pre>';
                        // echo $testname;
                        // echo '</pre>';
                        if (in_array(strtolower($testname), $search_array)) {
                            $sql4 = "select fldtestid from tblpatlabtest where fldstatus='Sampled'  and fldsampleid='" . trim($rowdata[0], '"') . "' and fldtestid='$testname'";

                            echo $sql4 . '<br/>';

                            $checkdata = DB::select(DB::raw($sql4));
                            //print_r($checkdata);
                            if ($checkdata) {
                                $sql = "UPDATE tblpatlabtest SET fldreportquanti=" . trim($rowdata[7], '"') . ",fldreportquali=" . trim($rowdata[7], '"') . ",fldstatus='Reported',fldtime_report=CURRENT_TIMESTAMP,xyz=0,fldsave_report=1,fldcomp_report='comp04',fldtestunit='SI'" . " WHERE fldsampleid='" . trim($rowdata[0], '"') . "' and fldtestid='" . $testname . "'";

                                $result = DB::update($sql);

                                echo 'Query run successfully: ' . $sql . '<br/>';
                            }
                        } else {
                            echo "No sample Id" . '<br/>';
                        }
                    }

                endif;
            } catch (\Exception $e) {
                //            dd($e);
            }
        }
    }

    public function machineInterfacingCron($machineName)
    {
        $mapping = array(
            'FT3' => 'Free Triiodothyronine',
            'FT4' => 'Free thyroxine',
            'TSH' => 'Thyyroid Stimulating Hormone',
            'PSA' => 'Prostate Specific Antigen',
            'Glu' => 'Glucose',
            'Ure' => 'Urea',
            'Crea' => 'Creatinine',
            'ALP' => 'Alkaline Phosphate',
            'ALT' => 'Alanine Transaminase',
            'SGPT' => 'Serum Glutamic Pyruvic Transaminase',
            'AST' => 'Aspartate transaminase',
            'SGOT' => 'serum glutamic-oxaloacetic transaminase',
            'TSB' => 'Total serum bilirubin',
            'DSB' => 'Direct Serum bilirubin',
            'BBT' => 'Bilirubin Total',
            'BBD' => 'Bilirubin Direct',
            'S.ALB' => 'Serum Albumin',
            'TP' => 'Total Protein',
            'GGT' => 'Gamma-GT',
            'CK-MB' => 'Creatine Kinase -MB',
            'CK-NAC' => 'Creatine Kinase -N',
            'CALL++' => 'Calcium',
            'IRON' => 'Iron',
            'AMY' => 'Amylase',
            'LIP' => 'Lipase',
            'TG' => 'Triglyceride',
            'CHOL' => 'Cholelsterol',
            'HDL' => 'High Density Lipoprotein',
            'LDL' => 'low Density Lipoprotein ',
            'LDH' => 'Lactate Dehydrogenase',
            'MAG' => 'Magnesium',
            'U/A' => 'Uric Acid',
            'FERIT' => 'Ferritin',

        );

        if ($machineName == "Biochem_siemens") {
            $machineDump = DumpTable::where('pat_lab_insert', 0)->where('machinename', $machineName)->get();
        } elseif ($machineName == "Biochem_sysmex2") {
        }

        $tbltests = DB::table('tbltest')->pluck('fldtestid')->toArray();

        if ($machineDump) {
            foreach ($machineDump as $dump) {
                $rowdata = preg_split("/[\t]/", $dump->dump);


                if ($rowdata) {
                    // print_r($rowdata); echo '<pre>';
                    $testname1 = trim($rowdata[3], '"');


                    if (array_key_exists($testname1, $mapping)) {
                        $testname = $mapping[$testname1];
                    } else {
                        $testname = $testname1;
                    }

                    $search_array = array_map('strtolower', $tbltests);

                    // echo '<pre>';
                    // echo $testname;
                    // echo '</pre>';
                    if (in_array(strtolower($testname), $search_array)) {
                        $sql4 = "";
                        $sql4 = "select fldtestid from tblpatlabtest where fldstatus='Sampled'  and fldsampleid='" . trim($rowdata[0], '"') . "' and fldtestid='$testname'";

                        echo $sql4 . '<br/>';

                        $checkdata = DB::select(DB::raw($sql4));
                        //print_r($checkdata);
                        if ($checkdata) {
                            $sql = "UPDATE tblpatlabtest SET fldreportquanti=" . trim($rowdata[7], '"') . ",fldreportquali=" . trim($rowdata[7], '"') . ",fldstatus='Reported',fldtime_report=CURRENT_TIMESTAMP,xyz=0,fldsave_report=1,fldcomp_report='comp04',fldtestunit='SI'" . " WHERE fldsampleid='" . trim($rowdata[0], '"') . "' and fldtestid='" . $testname . "'";

                            $result = DB::update($sql);

                            echo 'Query run successfully: ' . $sql . '<br/>';
                        }
                    } else {
                        echo "No sample Id" . '<br/>';
                    }
                }
            }
        }
    }

    public function machineInterfacingDepartment()
    {
        $mapping = array(
            'FT3' => 'Free Triiodothyronine',
            'FT4' => 'Free thyroxine',
            'TSH' => 'Thyyroid Stimulating Hormone',
            'PSA' => 'Prostate Specific Antigen',
            'Glu' => 'Glucose',
            'Ure' => 'Urea',
            'Crea' => 'Creatinine',
            'ALP' => 'Alkaline Phosphate',
            'ALT' => 'Alanine Transaminase',
            'SGPT' => 'Serum Glutamic Pyruvic Transaminase',
            'AST' => 'Aspartate transaminase',
            'SGOT' => 'serum glutamic-oxaloacetic transaminase',
            'TSB' => 'Total serum bilirubin',
            'DSB' => 'Direct Serum bilirubin',
            'BBT' => 'Bilirubin Total',
            'BBD' => 'Bilirubin Direct',
            'S.ALB' => 'Serum Albumin',
            'TP' => 'Total Protein',
            'GGT' => 'Gamma-GT',
            'CK-MB' => 'Creatine Kinase -MB',
            'CK-NAC' => 'Creatine Kinase -N',
            'CALL++' => 'Calcium',
            'IRON' => 'Iron',
            'AMY' => 'Amylase',
            'LIP' => 'Lipase',
            'TG' => 'Triglyceride',
            'CHOL' => 'Cholelsterol',
            'HDL' => 'High Density Lipoprotein',
            'LDL' => 'low Density Lipoprotein ',
            'LDH' => 'Lactate Dehydrogenase',
            'MAG' => 'Magnesium',
            'U/A' => 'Uric Acid',
            'FERIT' => 'Ferritin',

        );
        $machineName = '';
        $tbltests = DB::table('tbltest')->pluck('fldtestid')->toArray();
        $machines = DumpTable::distinct('machinename')->get('machinename');

        if ($machines) {
            foreach ($machines as $machine) {
                $machineName = $machine->machinename;
                $machineDump = DumpTable::where('pat_lab_insert', 0)->where('machinename', $machineName)->get();

                if ($machineName == "Biochem_siemens") {

                    if ($machineDump) {
                        foreach ($machineDump as $dump) {
                            $rowdata = preg_split("/[\t]/", $dump->dump);


                            if ($rowdata) {
                                // print_r($rowdata); echo '<pre>';
                                $testname1 = trim($rowdata[3], '"');


                                if (array_key_exists($testname1, $mapping)) {
                                    $testname = $mapping[$testname1];
                                } else {
                                    $testname = $testname1;
                                }

                                $search_array = array_map('strtolower', $tbltests);

                                // echo '<pre>';
                                // echo $testname;
                                // echo '</pre>';
                                if (in_array(strtolower($testname), $search_array)) {
                                    $sql4 = "";
                                    $sql4 = "select fldtestid from tblpatlabtest where fldstatus='Sampled'  and fldsampleid='" . trim($rowdata[0], '"') . "' and fldtestid='$testname'";

                                    echo $sql4 . '<br/>';

                                    $checkdata = DB::select(DB::raw($sql4));
                                    //print_r($checkdata);
                                    if ($checkdata) {
                                        $sql = "UPDATE tblpatlabtest SET fldreportquanti=" . trim($rowdata[7], '"') . ",fldreportquali=" . trim($rowdata[7], '"') . ",fldstatus='Reported',fldtime_report=CURRENT_TIMESTAMP,xyz=0,fldsave_report=1,fldcomp_report='comp04',fldtestunit='SI'" . " WHERE fldsampleid='" . trim($rowdata[0], '"') . "' and fldtestid='" . $testname . "'";

                                        $result = DB::update($sql);

                                        echo 'Query run successfully: ' . $sql . '<br/>';
                                    }
                                } else {
                                    echo "No sample Id" . '<br/>';
                                }
                            }
                        }
                    }
                } else {

                    if ($machineDump) {

                        foreach ($machineDump as $dump) {

                            $rowdata = preg_split("/[\t]/", $dump->dump);


                            if ($rowdata) {
                                //dd($rowdata);
                                $sampleid =  trim($rowdata[0], '"');
                                foreach ($rowdata as $rowtest) {
                                    $rowdatatest = explode('+',$rowtest);
                                     print_r($rowdatatest);
                                    $count = count($rowdatatest); 
                                    if (!empty($rowdatatest) && $count > 1) {
                                        $fldreportquali = trim($rowdatatest[1], '"');

                                        $testname1 = trim($rowdatatest[0], '"');


                                        if (array_key_exists($testname1, $mapping)) {
                                            $testname = trim($mapping[$testname1], '"');
                                        } else {
                                            $testname = trim($testname1, '"');
                                        }

                                        $search_array = array_map('strtolower', $tbltests);


                                         echo '<pre>';
                                         echo $testname;
                                         echo '</pre>';
                                         echo  $fldreportquali;
                                         echo '</pre>';
                                        if (in_array(strtolower($testname), $search_array)) {
                                            $sql4 = "";
                                            $sql4 = "select fldtestid from tblpatlabtest where fldstatus='Sampled'  and fldsampleid='" . $sampleid . "' and fldtestid='$testname'";

                                            echo $sql4 . '<br/>';

                                            $checkdata = DB::select(DB::raw($sql4));
                                            //print_r($checkdata);
                                            if ($checkdata) {
                                                $sql = "UPDATE tblpatlabtest SET fldreportquanti=" . $fldreportquali . ",fldreportquali=" . $fldreportquali . ",fldstatus='Reported',fldtime_report=CURRENT_TIMESTAMP,xyz=0,fldsave_report=1,fldcomp_report='comp04',fldtestunit='SI'" . " WHERE fldsampleid='" . $sampleid . "' and fldtestid='" . $testname . "'";

                                               // $result = DB::update($sql);

                                                echo 'Query run successfully: ' . $sql . '<br/>';
                                            }
                                        } else {
                                            echo "No sample Id" . '<br/>';
                                        }
                                    }
                                }
                                die();







                                // print_r($rowdata); echo '<pre>';

                            }
                        }
                    }
                }
            }
        }
    }
}
