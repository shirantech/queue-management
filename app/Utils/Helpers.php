<?php


namespace App\Utils;


use App\Http\Controllers\Nepali_Calendar;

class Helpers
{
    /**
     * Convert english date to nepali
     * @param $date
     * @return mixed
     */
    public static function dateEngToNep($date)
    {
        $nepali_calender = new Nepali_Calendar();
        $dateExplode = explode('-', $date);
        //dd($dateExplode);
        $date_day =$dateExplode[2];
        $date_month = $dateExplode[1];
        $date_year =$dateExplode[0];
        $date_nepali = $nepali_calender->eng_to_nep($date_year,$date_month,$date_day);

        return json_decode(json_encode($date_nepali));
    }
}
