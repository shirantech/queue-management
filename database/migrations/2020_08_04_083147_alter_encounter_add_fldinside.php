<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEncounterAddFldinside extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblencounter', function (Blueprint $table) {
            $table->boolean('fldinside')->nullable()->default(0);
            $table->boolean('fldphminside')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblencounter', function (Blueprint $table) {
            $table->dropColumn(['fldinside', 'fldphminside']);
        });
    }
}
