<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblDump extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_dump', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('machinename')->nullable()->default(null);
            $table->text('dump')->nullable()->default(null);
            $table->timestamp('rd')->useCurrent();
            $table->text('header')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_dump');
    }
}
