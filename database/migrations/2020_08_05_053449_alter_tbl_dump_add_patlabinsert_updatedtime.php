<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTblDumpAddPatlabinsertUpdatedtime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_dump', function (Blueprint $table) {
            $table->boolean('pat_lab_insert')->nullable()->default(0);
            $table->timestamp('updated_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_dump', function (Blueprint $table) {
            $table->dropColumn(['pat_lab_insert', 'updated_at']);
        });
    }
}
